import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SosReportPage } from './sos-report';
import { SelectSearchableModule } from '../../../node_modules/ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SosReportPage,
  ],
  imports: [
    IonicPageModule.forChild(SosReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class SosReportPageModule {}
