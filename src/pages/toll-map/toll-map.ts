import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, ModalController, Platform, ViewController, Navbar } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { MarkerCluster, GoogleMaps, Marker, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, Circle, MarkerClusterOptions } from '@ionic-native/google-maps';
import { TranslateService } from '@ngx-translate/core';
import { DrawerState } from 'ion-bottom-drawer';
import { Subscription } from 'rxjs/Subscription';
import { TollPage } from '../toll/toll';

@IonicPage()
@Component({
  selector: 'page-toll-map',
  templateUrl: 'toll-map.html',
})
export class TollMapPage implements OnInit, OnDestroy {

  navigateButtonColor: string = '#006EC5'; //Default Color
  navColor: string = '#fff';

  policeButtonColor: string = '#006EC5';
  policeColor: string = '#fff';

  petrolButtonColor: string = "#006EC5";
  petrolColor: string = '#fff';

  shouldBounce = true;
  dockedHeight = 80;
  distanceTop = 200;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 50;
  transition = '0.85s ease-in-out';

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: string = 'multiple';
  portstemp: any;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  resToken: string;
  liveDataShare: any;
  isEnabled: boolean = false;
  showMenuBtn: boolean = false;
  latlngCenter: any;
  mapHideTraffic: boolean = false;
  mapData: any = [];
  last_ping_on: any;
  geodata: any = [];
  geoShape: any = [];
  generalPolygon: any;
  locations: any = [];
  acModel: any;
  locationEndAddress: any;
  tempaddress: any;
  mapKey: string;
  shwBckBtn: boolean = false;
  recenterMeLat: any;
  recenterMeLng: any;
  menuActive: boolean;
  deviceDeatils: any = {};
  impkey: any;
  showaddpoibtn: boolean = false;
  isSuperAdmin: boolean;
  isDealer: boolean;
  zoomLevel: number = 18;
  voltage: number;
  temporaryMarker: any;
  temporaryInterval: any;
  circleObj: Circle;
  circleTimer: any;
  tempMarkArray: any = [];
  tempreture: number;
  door: number;
  nearbyPolicesArray: any = [];
  nearbyPetrolArray: any = [];
  socketurl: string = "https://www.oneqlik.in";
  displayNames: boolean = true;
  temporaryMarker123: Marker;
  t_veicle_count: any;
  saveMarkerCluster: MarkerCluster;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    // private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public plt: Platform,
    private viewCtrl: ViewController,
    private translate: TranslateService
  ) {
    if (localStorage.getItem('Total_Vech') !== null) {
      this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
    }
    //this.callBaseURL();
    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == this.translate.instant('Hybrid')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == this.translate.instant('Normal')) {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == this.translate.instant('Terrain')) {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == this.translate.instant('Satellite')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));
    this.menuActive = false;

    if (localStorage.getItem('DisplayVehicleName') !== null) {
      if (localStorage.getItem('DisplayVehicleName') == 'OFF') {
        this.displayNames = false;
      }
    }
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = {
    "car": this.carIcon,
    "bike": this.carIcon,
    "truck": this.carIcon,
    "bus": this.carIcon,
    "user": this.carIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon,
    "ambulance": this.carIcon,
    "auto": this.carIcon
  }

  resumeListener: Subscription = new Subscription();
  ionViewWillEnter() {
    if (this.plt.is('ios')) {
      this.shwBckBtn = true;
      this.viewCtrl.showBackButton(false);
    }

    this.plt.ready().then(() => {
      this.resumeListener = this.plt.resume.subscribe(() => {
        var today, Christmas;
        today = new Date();
        Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
        var diffMs = (today - Christmas); // milliseconds between now & Christmas
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        if (diffMins >= 5) {
          localStorage.removeItem("backgroundModeTime");
          this.alertCtrl.create({
            message: this.translate.instant("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
            buttons: [
              {
                text: this.translate.instant('YES PROCEED'),
                handler: () => {
                  this.refreshMe();
                }
              },
              {
                text: this.translate.instant('Back'),
                handler: () => {
                  this.navCtrl.setRoot('DashboardPage');
                }
              }
            ]
          }).present();
        }
      })
    })
  }

  ionViewWillLeave() {
    this.plt.ready().then(() => {
      this.resumeListener.unsubscribe();
    })
  }
  hideMe: boolean = false;
  @ViewChild(Navbar) navBar: Navbar;

  ionViewDidEnter() {
    this.navBar.backButtonClick = (ev: UIEvent) => {
      console.log('this will work in Ionic 3 +');
      this.hideMe = true;
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'back'
      });
    }
    if (localStorage.getItem("navigationFrom") === null) {
      this.initiateMap();  // at first we will load the map..
    }

    if (localStorage.getItem("CLICKED_ALL") != null) {
      this.showMenuBtn = true;
    }
    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") === 'live') {
        this.showMenuBtn = true;
      }
    }

    this.drawerState = DrawerState.Bottom;
    this.onClickShow = false;

    this.showBtn = false;
    this.SelectVehicle = "Select Vehicle";
    this.selectedVehicle = undefined;

  }

  ngOnInit() { }

  ngOnDestroy() {
    localStorage.removeItem("CLICKED_ALL");
  }

  initiateMap() {
    if (this.allData.map) {
      this.allData.map.remove();
      this.allData.map = this.newMap();
    } else {
      this.allData.map = this.newMap();
    }

    /* Check if traffic mode is On/Off */

    if (localStorage.getItem('TrafficMode') !== null) {
      if (localStorage.getItem('TrafficMode') === 'ON') {
        this.mapHideTraffic = true;
        this.allData.map.setTrafficEnabled(true);
      }
    }
    this.callFunction();

  }

  from: string;
  to: string;
  callFunction() {
    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();


    this.userDevices();


  }

  userDevices() {
    //var baseURLp;
    let that = this;

    var baseURLp = "https://www.oneqlik.in/toll/get"

    that.apiCall.startLoading().present();
    that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(resp => {
        that.apiCall.stopLoading();
        that.portstemp = resp;
        // console.log("list of vehicles :", that.portstemp)
        that.mapData = [];
        that.mapData = resp.map(function (d) {
          if (d.location !== undefined) {
            return { lat: d.location.coordinates['0'], lng: d.location.coordinates['1'] };
          }
        });

        var dummyData;

        dummyData = resp.map(function (d) {
          if (d.location === undefined)
            return;
          else {
            return {
              "position": {
                "lat": d.location.coordinates['0'],
                "lng": d.location.coordinates['1']
              },
              "name": d.tollName,
              "icon": {
                "url": that.getIconUrl(d),
                "size": {
                  "width": 20,
                  "height": 40
                }
              }
            };
          }
        });

        dummyData = dummyData.filter(function (element) {
          return element !== undefined;
        });

        console.log("dummy data: ", dummyData);
        // console.log("dummy data: ", this.dummyData());

        let bounds = new LatLngBounds(that.mapData);
        that.allData.map.moveCamera({
          target: bounds,
          zoom: 10
        })
        debugger
        if (that.isSuperAdmin || that.isDealer) {
          for (var t = 0; t < dummyData.length; t++) {
            console.log("check position: ", dummyData[t].position);
          }
          that.addCluster(dummyData);

        } else {
          if (resp.length > 10) {
            console.log("customers are greater than 10")
            that.addCluster(dummyData);
          } else {
            console.log("last cond");
          }
        }
      },
        err => {
          console.log(err);
        });
  }

  getIconUrl(data) {
    let that = this;
    var iconUrl;
    if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
      if (that.plt.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
      } else if (that.plt.is('android')) {
        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
      }
    } else {
      if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
        if (that.plt.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
        } else if (that.plt.is('android')) {
          iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
        }
      } else {
        var stricon;
        if (data.status.toLowerCase() === 'out of reach') {
          stricon = "outofreach";
          if (that.plt.is('ios')) {
            iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
          } else if (that.plt.is('android')) {
            iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
          }
        } else {
          if (that.plt.is('ios')) {
            iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
          } else if (that.plt.is('android')) {
            iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
          }
        }
      }
    }
    return iconUrl;
  }

  addCluster(data) {
    var small, large;
    if (this.plt.is('android')) {
      small = "./assets/markercluster/small.png";
      large = "./assets/markercluster/large.png";
    } else if (this.plt.is('ios')) {
      small = "www/assets/markercluster/small.png";
      large = "www/assets/markercluster/large.png";
    }
    let options: MarkerClusterOptions = {
      markers: data,
      icons: [
        {
          min: 3,
          max: 9,
          url: small,
          size: {
            height: 29,
            width: 29
          },
          label: {
            color: "white"
          }
        },
        {
          min: 10,
          url: large,
          size: {
            height: 37,
            width: 37
          },
          label: {
            color: "white"
          }
        }
      ],
      boundsDraw: false,
      maxZoomLevel: 15
    };
    // let markerCluster: MarkerCluster = this.allData.map.addMarkerClusterSync(options);

    this.allData.map.addMarkerCluster(options)
      .then((markercluster: MarkerCluster) => {
        markercluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
          let marker: Marker = params[1];
          marker.setTitle(marker.get("name"));
          marker.setSnippet(marker.get("address"));
          marker.showInfoWindow();
        });
        this.saveMarkerCluster = markercluster;
      });
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
      // this.ngOnDestroy();
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
      // this.someFunc();
    } else {
      if (maptype == 'TERRAIN') {
        // this.ngOnDestroy();
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
        // this.someFunc();
      } else {
        if (maptype == 'NORMAL') {
          // this.ngOnDestroy();
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
          // this.someFunc();
        }
      }
    }
  }

  newMap() {
    let mapOptions = {
      controls: {
        compass: false,
        zoom: false,
        myLocation: true,
        myLocationButton: false,
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      mapType: this.mapKey
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.plt.is('android')) {
      map.setPadding(20, 20, 20, 20);
    }
    return map;
  }

  goBack() {
    this.navCtrl.push(TollPage);
  }

  onClickMainMenu() {
    this.menuActive = !this.menuActive;
  }

  refreshMe() {
    this.ngOnDestroy();
    this.ionViewDidEnter();
  }
}
